#include <raven_gazebo_plugins/teleport_plugin.h>
#include <thread>

#include "ros/subscribe_options.h"

namespace gazebo {

TeleportPlugin::TeleportPlugin() {}

void TeleportPlugin::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf) {
  this->model = _model;

  std::string cmd_topic;

  if (_sdf->HasElement("world_frame")) {
    this->world_frame = _sdf->Get<std::string>("world_frame");
  } else {
    ROS_FATAL("Must specify <world_frame> for teleport_plugin");
  }

  if (_sdf->HasElement("cmd_topic")) {
    cmd_topic = _sdf->Get<std::string>("cmd_topic");
  } else {
    cmd_topic = "teleport_cmd";
  }

  // Initialize commanded pose.
  this->cmd_pose = this->model->WorldPose();
  auto pos = this->cmd_pose.Pos();
  auto rot = this->cmd_pose.Rot();

  if (!ros::isInitialized()) {
    ROS_FATAL("A ROS node for Gazebo has not been initialized.");
  }
  this->ros_nh.reset(new ros::NodeHandle());

  // Set up ROS subscriber + thread.
  std::string topic = this->model->GetName() + "/" + cmd_topic;
  int queue_size = 1;
  auto cb = boost::bind(&TeleportPlugin::OnRosMsg, this, _1);
  ros::SubscribeOptions so =
    ros::SubscribeOptions::create<geometry_msgs::PoseStamped>(
        topic, queue_size, cb, ros::VoidPtr(), &this->ros_queue);
  std::cout << "Subscribing to topic: " << topic << std::endl;
  this->ros_sub = this->ros_nh->subscribe(so);
  this->ros_queue_thread = std::thread(std::bind(&TeleportPlugin::QueueThread, this));

  this->tf_listener.reset(new tf::TransformListener());
  this->tf_listener->setExtrapolationLimit(ros::Duration(1.0));
}

void TeleportPlugin::Init() {
  this->update_connection = event::Events::ConnectWorldUpdateBegin(
      std::bind(&TeleportPlugin::OnUpdate, this));
}

void TeleportPlugin::OnRosMsg(const geometry_msgs::PoseStampedConstPtr &_msg) {
  geometry_msgs::PoseStamped transformed_pose;
  try {
    this->tf_listener->transformPose(this->world_frame, *_msg, transformed_pose);
  } catch (tf::TransformException ex) {
    ROS_WARN_STREAM("TeleportPlugin: " << ex.what());
    return;
  }

  auto pp = transformed_pose.pose.position;
  auto pose = ignition::math::Vector3(pp.x, pp.y, pp.z);
  auto oo = transformed_pose.pose.orientation;
  auto quat = ignition::math::Quaternion(oo.w, oo.x, oo.y, oo.z);
  this->cmd_pose.Set(pose, quat);
}

void TeleportPlugin::OnUpdate() {
  this->model->SetWorldPose(this->cmd_pose);
}

void TeleportPlugin::QueueThread() {
  static const double timeout = 0.01;
  while (this->ros_nh->ok()) {
    this->ros_queue.callAvailable(ros::WallDuration(timeout));
  }
}

GZ_REGISTER_MODEL_PLUGIN(TeleportPlugin)
}
