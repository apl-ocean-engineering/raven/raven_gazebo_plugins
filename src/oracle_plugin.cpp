#include <raven_gazebo_plugins/oracle_plugin.h>

#include "ros/subscribe_options.h"

namespace gazebo {

OraclePlugin::OraclePlugin() {}

void OraclePlugin::Load(physics::ModelPtr model, sdf::ElementPtr sdf) {
  this->model = model;

  std::cout << "Loading Oracle Plugin!" << std::endl;

  if (sdf->HasElement("updateRate")) {
    this->update_rate = sdf->GetElement("updateRate")->Get<double>();
  } else {
    ROS_WARN("No updateRate specified. Defaulting to 0.0 (as fast as possible)");
    this->update_rate = 0.0;
  }

  world_frame_ = "";
  if (sdf->HasElement("world_frame")) {
    world_frame_ = sdf->Get<std::string>("world_frame");
    ROS_WARN_STREAM("Got world_frame: " << world_frame_);
  } else {
    ROS_FATAL("Must specify <world_frame> for oracle_plugin");
  }

  odom_frame_ = "";
  if (sdf->HasElement("odom_frame")) {
    odom_frame_ = sdf->Get<std::string>("odom_frame");
    ROS_WARN_STREAM("Got odom_frame: " << odom_frame_);
  } else {
    ROS_FATAL("Must specify <odom_frame> for oracle_plugin");
  }

  vehicle_frame_ = "";
  if (sdf->HasElement("vehicle_frame")) {
    vehicle_frame_ = sdf->Get<std::string>("vehicle_frame");
    ROS_WARN_STREAM("Got vehicle frame: " << vehicle_frame_);
  } else {
    ROS_FATAL("Must specify <vehicle_frame> for oracle_plugin");
  }

  this->prev_time = this->model->GetWorld()->SimTime();

  if (!ros::isInitialized()) {
    ROS_FATAL("A ROS node for Gazebo has not been initialized.");
  }
  this->ros_nh.reset(new ros::NodeHandle());
  this->br.reset(new tf2_ros::TransformBroadcaster());

  // Set up ROS publisher
  std::string topic = this->model->GetName() + "/odom";
  int queue_size = 1;
  this->odom_pub = this->ros_nh->advertise<nav_msgs::Odometry>(topic, queue_size);

  tf_listener_.reset(new tf::TransformListener());
  tf_listener_->setExtrapolationLimit(ros::Duration(1.0));
}

void OraclePlugin::Init() {
  this->update_connection = event::Events::ConnectWorldUpdateBegin(
      std::bind(&OraclePlugin::OnUpdate, this));
}

void OraclePlugin::OnUpdate() {
  common::Time curr_time = this->model->GetWorld()->SimTime();
  double dt = (curr_time - this->prev_time).Double();
  if(this->update_rate > 0 && dt < (1.0 / this->update_rate)) {
    return;
  } else {
    this->prev_time = curr_time;
  }

  auto pose = this->model->WorldPose();
  auto pos = pose.Pos();
  auto rot = pose.Rot();

  ros::Time ros_time;
  ros_time.fromSec(curr_time.Float());

  // Publish tf
  geometry_msgs::TransformStamped tf_msg;
  tf_msg.header.stamp = ros_time;
  tf_msg.header.frame_id = world_frame_;
  tf_msg.child_frame_id = vehicle_frame_;
  tf_msg.transform.translation.x = pos.X();
  tf_msg.transform.translation.y = pos.Y();
  tf_msg.transform.translation.z = pos.Z();
  tf_msg.transform.rotation.x = rot.X();
  tf_msg.transform.rotation.y = rot.Y();
  tf_msg.transform.rotation.z = rot.Z();
  tf_msg.transform.rotation.w = rot.W();
  this->br->sendTransform(tf_msg);

  // Publish Odometry
  // First, create a PoseStamped for easy transformations with tf
  geometry_msgs::PoseStamped gazebo_pose;
  geometry_msgs::PoseStamped odom_pose;
  gazebo_pose.header.frame_id = world_frame_;
  gazebo_pose.header.stamp = ros_time;
  gazebo_pose.pose.position.x = pos.X();
  gazebo_pose.pose.position.y = pos.Y();
  gazebo_pose.pose.position.z = pos.Z();
  gazebo_pose.pose.orientation.x = rot.X();
  gazebo_pose.pose.orientation.y = rot.Y();
  gazebo_pose.pose.orientation.z = rot.Z();
  gazebo_pose.pose.orientation.w = rot.W();
  try {
    tf_listener_->transformPose(odom_frame_, gazebo_pose, odom_pose);
  } catch (tf::TransformException ex) {
    ROS_WARN_STREAM("OraclePlugin: " << ex.what());
    return;
  }

  nav_msgs::Odometry odom_msg;
  odom_msg.header.stamp = ros_time;
  odom_msg.header.frame_id = odom_frame_;
  odom_msg.child_frame_id = vehicle_frame_;
  odom_msg.pose.pose.position.x = odom_pose.pose.position.x;
  odom_msg.pose.pose.position.y = odom_pose.pose.position.y;
  odom_msg.pose.pose.position.z = odom_pose.pose.position.z;
  odom_msg.pose.pose.orientation.x = odom_pose.pose.orientation.x;
  odom_msg.pose.pose.orientation.y = odom_pose.pose.orientation.y;
  odom_msg.pose.pose.orientation.z = odom_pose.pose.orientation.z;
  odom_msg.pose.pose.orientation.w = odom_pose.pose.orientation.w;
  // WARNING: twist isn't filled out, so plotting it in rviz will look funky.
  this->odom_pub.publish(odom_msg);

}

GZ_REGISTER_MODEL_PLUGIN(OraclePlugin)
}
