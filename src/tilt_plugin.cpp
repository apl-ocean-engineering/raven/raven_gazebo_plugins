#include <raven_gazebo_plugins/tilt_plugin.h>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <thread>

#include "ros/subscribe_options.h"

namespace gazebo {


TiltPlugin::TiltPlugin() {}

void TiltPlugin::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf) {
  this->model = _model;

  this->joint = _model->GetJoints()[0];
  std::cerr << "\n Loading TiltPlugin!!\n";
  std::cerr << "Got joint: " << this->joint->GetScopedName() << std::endl;
  std::cerr << "  - Parent: " << this->joint->GetParent()->GetScopedName() << std::endl;
  std::cerr << "  - Child: " << this->joint->GetChild()->GetScopedName() << std::endl;
  std::cerr << "  - num children: " << this->joint->GetChildCount() << std::endl;

  // Default to straight ahead
  double initial_position = 0.0;
  if (_sdf->HasElement("initial_position")) {
    initial_position = _sdf->Get<double>("initial_position");
    std::cerr << "Got initial position from SDF: " << initial_position << std::endl;
  }
  this->cmd_radians = initial_position;

  std::string controlled_joint = "";
  if (_sdf->HasElement("controlled_joint")) {
    controlled_joint = _sdf->Get<std::string>("controlled_joint");
    std::cerr << "Got controlled joint from SDF: " << controlled_joint << std::endl;
  }

  double foo = 1.0;
  if (_sdf->HasElement("rotation_rate")) {
    foo = _sdf->Get<double>("rotation_rate");
    std::cerr << "Got angular rate from SDF: " << foo << std::endl;
  } else {
    std::cerr << "Defaulting to rotation rate: " << foo << " rad/s\n";
  }
  this->max_rate = foo;

  std::string joint_name = this->model->GetScopedName() + "::" + controlled_joint;
  std::cout << "Looking for joint: " << joint_name << std::endl;
  this->joint = _model->GetJoint(joint_name);
  //this->joint = _model->GetJoints()[0];
  this->model->GetJointController()->SetJointPosition(
    this->joint->GetScopedName(), initial_position);
  this->min_radians = this->joint->LowerLimit();
  this->max_radians = this->joint->UpperLimit();

  // Setup for ROS interfaces
  // TODO: Apparently this is deprecated? newer documentation:
  //    http://gazebosim.org/tutorials?tut=ros_plugins&cat=connect_ros
  //    says that ros::init should only ever be called by gazebo_ros_api_plugin.cpp
  if (!ros::isInitialized()) {
    int argc = 0;
    char **argv = NULL;
    ros::init(argc, argv, "gazebo_client", ros::init_options::NoSigintHandler);
  }
  this->ros_nh.reset(new ros::NodeHandle("gazebo_client"));

  ros::SubscribeOptions position_so =
    ros::SubscribeOptions::create<std_msgs::Float32>(
      "/" + this->model->GetName() + "/tilt_angle_cmd",
      1,
      boost::bind(&TiltPlugin::OnPositionMsg, this, _1),
      ros::VoidPtr(), &this->ros_queue);
  this->position_sub = this->ros_nh->subscribe(position_so);

  ros::SubscribeOptions direction_so =
    ros::SubscribeOptions::create<greensea_msgs::TiltCmd>(
      "/" + this->model->GetName() + "/tilt_dir_cmd",
      1,
      boost::bind(&TiltPlugin::OnDirectionMsg, this, _1),
      ros::VoidPtr(), &this->ros_queue);
  this->direction_sub = this->ros_nh->subscribe(direction_so);

  this->ros_queue_thread = std::thread(std::bind(&TiltPlugin::QueueThread, this));
}

void TiltPlugin::Init() {
  this->update_connection = event::Events::ConnectWorldUpdateBegin(
    std::bind(&TiltPlugin::OnUpdate, this));
}

double TiltPlugin::ThresholdCommand(double cmd) {
  double thresholded = std::min(this->max_radians,
                               std::max(this->min_radians, cmd));
  return thresholded;
}

void TiltPlugin::OnPositionMsg(const std_msgs::Float32ConstPtr &msg) {
  std::cout << "Got position command: " << msg->data << std::endl;
  this->cmd_radians = ThresholdCommand(msg->data);
}

void TiltPlugin::OnDirectionMsg(const greensea_msgs::TiltCmdConstPtr &_msg) {
  // TODO: This is an absolute hack, relying on the joystick to publish
  //       at a maximum rate of 10Hz. However, I think this goes along
  //       with the rest of the plugin's design ...
  double cmd = this->cmd_radians;
  if (_msg->tilt_down) {
    cmd -= 0.1 * this->max_rate;
  } else if (_msg->tilt_up) {
    cmd += 0.1 * this->max_rate;
  }
  this->cmd_radians = ThresholdCommand(cmd);
}

void TiltPlugin::QueueThread() {
  static const double timeout = 0.01;
  while(this->ros_nh->ok()) {
    this->ros_queue.callAvailable(ros::WallDuration(timeout));
  }
}

void TiltPlugin::OnUpdate() {
  // Need to continually set joint to the commanded angle
  // or gravity will take over.
  // The alternative to this is to add a PID controller on the joint.
  this->model->GetJointController()->SetJointPosition(
      this->joint->GetScopedName(), cmd_radians);
}

GZ_REGISTER_MODEL_PLUGIN(TiltPlugin)
}  // namespace gazebo

