#ifndef _ORACLE_PLUGIN_HH_
#define _ORACLE_PLUGIN_HH_

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
//#include "gazebo/math/Pose.hh"
#include <ignition/math/Pose3.hh>
#include <ignition/math/Quaternion.hh>
#include <ignition/math/Vector3.hh>

#include "ros/ros.h"
#include "geometry_msgs/TransformStamped.h"
#include "nav_msgs/Odometry.h"
#include "tf2_ros/transform_broadcaster.h"
#include <tf/transform_listener.h>

namespace gazebo {
class OraclePlugin : public ModelPlugin {
 public:
  OraclePlugin();

  virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);
  virtual void Init();

 protected:
  virtual void OnUpdate();

  event::ConnectionPtr update_connection;

 private:
  void QueueThread();
  physics::ModelPtr model;
  // Gazebo's world frame (aligned with ENU)
  std::string world_frame_;
  // Frame that Odom should be expressed in.
  std::string odom_frame_;
  // Frame whose position we care about.
  // It's position relative to gazebo_frame will be published to tf,
  // and relative to odom_frame will be published to odom.
  std::string vehicle_frame_;

  std::unique_ptr<ros::NodeHandle> ros_nh;
  ros::Publisher odom_pub;
  double update_rate;  // Hz
  common::Time prev_time;
  std::unique_ptr<tf2_ros::TransformBroadcaster> br;
  std::unique_ptr<tf::TransformListener> tf_listener_;


};  // class Oracle Plugin
}  // namespace Gazebo

#endif  // _ORACLE_PLUGIN_HH_
