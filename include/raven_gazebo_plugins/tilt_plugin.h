#ifndef _TILT_PLUGIN_HH_
#define _TILT_PLUGIN_HH_

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>

#include "ros/ros.h"
#include "ros/callback_queue.h"
#include "greensea_msgs/TiltCmd.h"
#include "std_msgs/Float32.h"

namespace gazebo {
  class TiltPlugin : public ModelPlugin {
    public: TiltPlugin();

    public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);
    public: virtual void Init();
    public: void OnPositionMsg(const std_msgs::Float32ConstPtr &_msg);
    public: void OnDirectionMsg(const greensea_msgs::TiltCmdConstPtr &_msg);

    protected: virtual void OnUpdate();

    // Used for subscribing to Gazebo's update events
    protected: event::ConnectionPtr update_connection;

    private: void QueueThread();
    // threshold command based on joint angles from the URDF
    private: double ThresholdCommand(double cmd);

    // Model that the plugin is attached to
    private: physics::ModelPtr model;
    // Joint that the plugin is controlling
    private: physics::JointPtr joint;

    // Required for connecting to ROS
    // Since we can't just call spin() within a Gazebo plugin, this has to
    // handle the ROS queue manually.
    private: std::unique_ptr<ros::NodeHandle> ros_nh;
    // Subscriber for absolute position commands
    private: ros::Subscriber position_sub;
    // Subscriber for relative position commands
    private: ros::Subscriber direction_sub;
    private: ros::CallbackQueue ros_queue;
    private: std::thread ros_queue_thread;
    private: double cmd_radians;
    // Max rotation rate of plugin, rad/sec. Only applies to direction commands
    private: double max_rate;
    // Joint limits. Pulled in from the URDF, not parameters to the plugin.
    private: double min_radians;
    private: double max_radians;
  };
}

#endif
