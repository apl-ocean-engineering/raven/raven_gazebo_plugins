#ifndef _TELEPORT_PLUGIN_HH_
#define _TELEPORT_PLUGIN_HH_

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
//#include "gazebo/math/Pose.hh"
#include <ignition/math/Pose3.hh>
#include <ignition/math/Quaternion.hh>
#include <ignition/math/Vector3.hh>

#include "ros/ros.h"
#include "ros/callback_queue.h"
#include <tf/transform_listener.h>
#include "geometry_msgs/PoseStamped.h"

namespace gazebo {
class TeleportPlugin : public ModelPlugin {
 public:
  TeleportPlugin();

  virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);
  virtual void Init();
  void OnRosMsg(const geometry_msgs::PoseStampedConstPtr &_msg);

 protected:
  virtual void OnUpdate();

  event::ConnectionPtr update_connection;

 private:
  void QueueThread();
  physics::ModelPtr model;
  std::unique_ptr<ros::NodeHandle> ros_nh;
  std::unique_ptr<tf::TransformListener> tf_listener;
  ros::Subscriber ros_sub;
  ros::CallbackQueue ros_queue;
  std::thread ros_queue_thread;
  // Gazebo's world frame (aligned with ENU)
  std::string world_frame;
  ignition::math::Pose3<double> cmd_pose;

};  // class Teleport Plugin
}  // namespace Gazebo



#endif  // _TELEPORT_PLUGIN_HH_
